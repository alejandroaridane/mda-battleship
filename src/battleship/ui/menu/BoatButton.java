/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package battleship.ui.menu;

import javax.swing.JToggleButton;

/**
 *
 * @author Usuario
 */
public class BoatButton extends JToggleButton{
    private String name="Barco";
    private int size=0;

    public BoatButton(String name,int size) {
        this.name=name;
        this.size=size;
    }

    public int getBoatSize() {
        return size;
    } 
    
}

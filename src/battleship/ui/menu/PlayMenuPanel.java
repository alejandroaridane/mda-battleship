package battleship.ui.menu;

import javax.swing.JButton;

public class PlayMenuPanel extends javax.swing.JPanel {

    public PlayMenuPanel() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        localMatchButton = new javax.swing.JButton();
        iaMatchButton = new javax.swing.JButton();
        onlineMatchButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(153, 204, 255));
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setPreferredSize(new java.awt.Dimension(600, 400));

        localMatchButton.setFont(new java.awt.Font("Andalus", 0, 24)); // NOI18N
        localMatchButton.setText("Play Local Match");

        iaMatchButton.setFont(new java.awt.Font("Andalus", 0, 24)); // NOI18N
        iaMatchButton.setText("Play against IA");

        onlineMatchButton.setFont(new java.awt.Font("Andalus", 0, 24)); // NOI18N
        onlineMatchButton.setText("Play Online");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(186, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(onlineMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(iaMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(localMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(172, 172, 172))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(localMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(iaMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(onlineMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton iaMatchButton;
    private javax.swing.JButton localMatchButton;
    private javax.swing.JButton onlineMatchButton;
    // End of variables declaration//GEN-END:variables

    public JButton getIaMatchButton() {
        return iaMatchButton;
    }

    public JButton getLocalMatchButton() {
        return localMatchButton;
    }

    public JButton getOnlineMatchButton() {
        return onlineMatchButton;
    }
}

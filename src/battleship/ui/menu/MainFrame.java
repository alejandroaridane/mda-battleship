package battleship.ui.menu;

import java.awt.Point;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JPanel;
import map.MyMap;
import map.MyTile;

public class MainFrame extends javax.swing.JFrame {
    
    String player1Name;
    String player2Name;

    public MainFrame() {
        initComponents();        

        initPlayMenuPanelEvents();        
        initPlayer1DeploymentPanelEvents();  //Need ship's buttons events for drag and drop
       
    }

    private void initPlayMenuPanelEvents() {
        playMenuPanel.getLocalMatchButton().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                System.out.println("localMatchButton pressed");
                changeToLocalMatchPanelFromMainMenu();
            }
        });
    }
    
    private void initPlayer1DeploymentPanelEvents() {
       localPlayer1DeploymentPanel1.getBackButtonFromLocalplayer1Deployment().addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               System.out.println("Back from player1 deployment");
               changeToMainMenuPanelFromLocalPlayer1Deployment();    
           }
       });
    }
    
    public void initLocalMatchMenuPanelEvents(ActionListener newGameListener) {
        localMatchMenuPanel.getPlayLocalMatchButton().addActionListener(newGameListener);

        localMatchMenuPanel.getBackFromLocalPanelButton().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                System.out.println("Back from local match");
                getLocalMatchMenuPanel().getPlayer1LocalTextField().setText("");
                getLocalMatchMenuPanel().getPlayer2LocalTextField().setText("");
                changeToMainMenuPanelFromLocalMatch();
            }
        });
    }

    public LocalMatchMenuPanel getLocalMatchMenuPanel() {
        return localMatchMenuPanel;
    }

    public LocalPlayer1DeploymentPanel getLocalPlayer1DeploymentPanel1() {
        return localPlayer1DeploymentPanel1;
    }

    public PlayMenuPanel getPlayMenuPanel() {
        return playMenuPanel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        playMenuPanel = new battleship.ui.menu.PlayMenuPanel();
        localMatchMenuPanel = new battleship.ui.menu.LocalMatchMenuPanel();
        localPlayer1DeploymentPanel1 = new battleship.ui.menu.LocalPlayer1DeploymentPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Battleship Main Menu");
        setPreferredSize(new java.awt.Dimension(605, 428));
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(playMenuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(localMatchMenuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(localPlayer1DeploymentPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(playMenuPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(localMatchMenuPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(localPlayer1DeploymentPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame mainMenuFrame= new MainFrame();                
                mainMenuFrame.setVisible(true);
                mainMenuFrame.setLocationRelativeTo(null);
                
            }
        });
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private battleship.ui.menu.LocalMatchMenuPanel localMatchMenuPanel;
    private battleship.ui.menu.LocalPlayer1DeploymentPanel localPlayer1DeploymentPanel1;
    private battleship.ui.menu.PlayMenuPanel playMenuPanel;
    // End of variables declaration//GEN-END:variables
    
    MyMap tileMap;
    HashMap<Integer, MyTile> map;

    public HashMap<Integer, MyTile> getMap() {
        return map;
    }
    
    
    //change panels block 
    public void changeToMainMenuPanelFromLocalMatch(){
        playMenuPanel.setVisible(true);
        localMatchMenuPanel.setVisible(false);
    }
    
    public void changeToLocalMatchPanelFromMainMenu(){
        playMenuPanel.setVisible(false);
        localMatchMenuPanel.setVisible(true);
    }
    
    public void changeToLocalPlayer1Deployment(){
        localMatchMenuPanel.setVisible(false);
        localPlayer1DeploymentPanel1.setVisible(true);
    }
    
    public void changeToMainMenuPanelFromLocalPlayer1Deployment(){
        localPlayer1DeploymentPanel1.setVisible(false);
        playMenuPanel.setVisible(true);
    }
    
    //Get some variables from panels
    public String getPlayer1Name(){
        player1Name = localMatchMenuPanel.getPlayer1LocalTextField().getText();
        return player1Name;
    } 
    
    public String getPlayer2Name(){
        player2Name = localMatchMenuPanel.getPlayer2LocalTextField().getText();
        return player2Name;
    }
    
    public JPanel getDeploymentMapFromPlayer1(){  //ALEJANDRO ESTA PARTE ES PARA OBTENER TU PANEL DE DESPLIEGUE
        return localPlayer1DeploymentPanel1.getPlayer1deploymentPanel();
    }
    
    public int getOrientation(){   // ALEJANDRO ESTO ES PARA OBTENER LA ORIENTAION SELESIONADA 1=up 2=rigth 3=down 4=left
        return localPlayer1DeploymentPanel1.getSelectedOrientation();        
    }
    
    public void initDeploymentMapForPlayer1(){
        tileMap = new MyMap();
   
        map = tileMap.createMap(tileMap.getClass().getResource("img/water.png").getFile(),tileMap.getClass().getResource("img/cover.png").getFile());

        setDeploymentMapForPlayer1(tileMap);
    }
    
    public void setDeploymentMapForPlayer1(MyMap deploymentPanel){  //ADRIAN ESTA PARTE ES PARA QUE INYETES TU PANEL
        localPlayer1DeploymentPanel1.setDeploymentPanel(deploymentPanel);
        localPlayer1DeploymentPanel1.setVisible(true);
        localPlayer1DeploymentPanel1.createControlMap();
    } 
    
    //Set events from controllers block
    public void setLocalMatchButtonListener(ActionListener newGameListener) {
        localMatchMenuPanel.getPlayLocalMatchButton().addActionListener(newGameListener);
        localPlayer1DeploymentPanel1.getPlayer1NameLabel().setText(player1Name);
    }

}

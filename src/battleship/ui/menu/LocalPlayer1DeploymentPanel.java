package battleship.ui.menu;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import map.MyControlMap;
import map.MyMap;
import map.MyTile;

public class LocalPlayer1DeploymentPanel extends javax.swing.JPanel {
    public int selectedOrientation=1;
    public ButtonGroup boatGroup= new ButtonGroup();
    
    public LocalPlayer1DeploymentPanel() {
        initComponents();
        upRotateButton.setSelected(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        player1NameLabel = new javax.swing.JLabel();
        backButtonFromLocalplayer1Deployment = new javax.swing.JButton();
        goToPlayer2DeploymentButton = new javax.swing.JButton();
        rotateControlsLabel = new javax.swing.JLabel();
        rotateControlPanel = new javax.swing.JPanel();
        upRotateButton = new javax.swing.JToggleButton();
        downRotateButton = new javax.swing.JToggleButton();
        leftRotateButton = new javax.swing.JToggleButton();
        rigthRotateButton = new javax.swing.JToggleButton();
        shipsDeploymentPanel = new javax.swing.JPanel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        player1deploymentPanel = new javax.swing.JPanel();
        player1ControlPanel = new javax.swing.JPanel();

        setBackground(new java.awt.Color(153, 204, 255));
        setPreferredSize(new java.awt.Dimension(600, 400));

        player1NameLabel.setFont(new java.awt.Font("Andalus", 1, 18)); // NOI18N
        player1NameLabel.setText("Player1 Name");

        backButtonFromLocalplayer1Deployment.setFont(new java.awt.Font("Andalus", 0, 15)); // NOI18N
        backButtonFromLocalplayer1Deployment.setText("Back to Main Menu");

        goToPlayer2DeploymentButton.setFont(new java.awt.Font("Andalus", 0, 18)); // NOI18N
        goToPlayer2DeploymentButton.setLabel("Im ready, turn for player2 deployment");

        rotateControlsLabel.setFont(new java.awt.Font("Andalus", 0, 14)); // NOI18N
        rotateControlsLabel.setText("Rotate Controls");

        upRotateButton.setText("Up");
        upRotateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upRotateButtonActionPerformed(evt);
            }
        });

        downRotateButton.setText("Down");
        downRotateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downRotateButtonActionPerformed(evt);
            }
        });

        leftRotateButton.setText("Left");
        leftRotateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftRotateButtonActionPerformed(evt);
            }
        });

        rigthRotateButton.setText("Right");
        rigthRotateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rigthRotateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout rotateControlPanelLayout = new javax.swing.GroupLayout(rotateControlPanel);
        rotateControlPanel.setLayout(rotateControlPanelLayout);
        rotateControlPanelLayout.setHorizontalGroup(
            rotateControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rotateControlPanelLayout.createSequentialGroup()
                .addGroup(rotateControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rotateControlPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(leftRotateButton)
                        .addGap(8, 8, 8)
                        .addComponent(rigthRotateButton))
                    .addGroup(rotateControlPanelLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(rotateControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(downRotateButton)
                            .addComponent(upRotateButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        rotateControlPanelLayout.setVerticalGroup(
            rotateControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rotateControlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(upRotateButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(rotateControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rigthRotateButton)
                    .addComponent(leftRotateButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downRotateButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipsDeploymentPanel.setPreferredSize(new java.awt.Dimension(20, 20));
        shipsDeploymentPanel.setLayout(new java.awt.GridLayout(6, 2, 0, 1));

        player1deploymentPanel.setBackground(new java.awt.Color(0, 153, 255));
        player1deploymentPanel.setPreferredSize(new java.awt.Dimension(350, 350));
        player1deploymentPanel.setLayout(new java.awt.GridLayout(1, 2));

        player1ControlPanel.setOpaque(false);
        player1ControlPanel.setPreferredSize(new java.awt.Dimension(350, 350));
        player1ControlPanel.setLayout(new java.awt.GridLayout(1, 2));

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 432, Short.MAX_VALUE)
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 41, Short.MAX_VALUE)
                    .addComponent(player1ControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 41, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 41, Short.MAX_VALUE)
                    .addComponent(player1deploymentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 41, Short.MAX_VALUE)))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 358, Short.MAX_VALUE)
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(player1ControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 8, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(player1deploymentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 8, Short.MAX_VALUE)))
        );
        jLayeredPane1.setLayer(player1deploymentPanel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(player1ControlPanel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(player1NameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(rotateControlsLabel))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(backButtonFromLocalplayer1Deployment)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(shipsDeploymentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(rotateControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(32, 32, 32)
                .addComponent(goToPlayer2DeploymentButton, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 168, Short.MAX_VALUE)
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(player1NameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipsDeploymentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(rotateControlsLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rotateControlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(goToPlayer2DeploymentButton, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(backButtonFromLocalplayer1Deployment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(55, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rigthRotateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rigthRotateButtonActionPerformed
        selectedOrientation=2;
        leftRotateButton.setSelected(false);
        downRotateButton.setSelected(false);
        upRotateButton.setSelected(false);
        System.out.println("rotating to rigth");
    }//GEN-LAST:event_rigthRotateButtonActionPerformed

    private void leftRotateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftRotateButtonActionPerformed
        selectedOrientation=4;
        rigthRotateButton.setSelected(false);
        downRotateButton.setSelected(false);
        upRotateButton.setSelected(false);
        System.out.println("rotating to left");
    }//GEN-LAST:event_leftRotateButtonActionPerformed

    private void downRotateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downRotateButtonActionPerformed
        selectedOrientation=3;
        leftRotateButton.setSelected(false);
        upRotateButton.setSelected(false);
        rigthRotateButton.setSelected(false);
        System.out.println("rotating down");
    }//GEN-LAST:event_downRotateButtonActionPerformed

    private void upRotateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upRotateButtonActionPerformed
        selectedOrientation=1;
        leftRotateButton.setSelected(false);
        downRotateButton.setSelected(false);
        rigthRotateButton.setSelected(false);
        System.out.println("rotating up");
    }//GEN-LAST:event_upRotateButtonActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButtonFromLocalplayer1Deployment;
    private javax.swing.JToggleButton downRotateButton;
    private javax.swing.JButton goToPlayer2DeploymentButton;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JToggleButton leftRotateButton;
    private javax.swing.JPanel player1ControlPanel;
    private javax.swing.JLabel player1NameLabel;
    private javax.swing.JPanel player1deploymentPanel;
    private javax.swing.JToggleButton rigthRotateButton;
    private javax.swing.JPanel rotateControlPanel;
    private javax.swing.JLabel rotateControlsLabel;
    private javax.swing.JPanel shipsDeploymentPanel;
    private javax.swing.JToggleButton upRotateButton;
    // End of variables declaration//GEN-END:variables
   
    
    public JButton getBackButtonFromLocalplayer1Deployment() {
        return backButtonFromLocalplayer1Deployment;
    }

    public JButton getGoToPlayer2DeploymentButton() {
        return goToPlayer2DeploymentButton;
    }

    public JLabel getPlayer1NameLabel() {
        return player1NameLabel;
    }

    public JPanel getPlayer1deploymentPanel() {
        return player1deploymentPanel;
    }
    
    public int getSelectedOrientation(){
        return selectedOrientation;
    }
    
    private HashMap<Integer, MyTile> tileMap= new HashMap<Integer, MyTile>();

    public HashMap<Integer, MyTile> getTileMap() {
        return tileMap;
    }
    
    
    
    public void setDeploymentPanel(MyMap deploymentPanel){
        this.player1deploymentPanel.add(deploymentPanel);
        this.player1deploymentPanel.setVisible(true);
        tileMap= deploymentPanel.getTileMap();
    }
    
   
    
    public void createControlMap(){
        MyControlMap map = new MyControlMap();
        map.createControlMap(tileMap,this);
        map.setVisible(true);
        
        player1ControlPanel.add(map);
        player1ControlPanel.setVisible(true);
    }
    
    public void initializeBoats(ArrayList<Integer> sizeBoatsToDeploy,ArrayList<String> nameBoatsToDeploy){
         int i=0;
         for(Integer boatsSize: sizeBoatsToDeploy){
             BoatButton boat=new BoatButton(nameBoatsToDeploy.get(i), boatsSize);
             boat.setText(nameBoatsToDeploy.get(i));
             shipsDeploymentPanel.add(boat);
             boatGroup.add(boat);
             i++;
         }
    }
    

    //FALTAN GETTERS DE BOTONES DE BARCOS Y EN EL MAIN FRAME INYECTARLE EL EVENTO A CADA BOTON DE BARCO

}

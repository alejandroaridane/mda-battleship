package battleship.ui.menu;

import javax.swing.JButton;
import javax.swing.JTextField;

public class LocalMatchMenuPanel extends javax.swing.JPanel {

    public LocalMatchMenuPanel() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        player1LocalLabel = new javax.swing.JLabel();
        player2LocalLabel = new javax.swing.JLabel();
        player1LocalTextField = new javax.swing.JTextField();
        player2LocalTextField = new javax.swing.JTextField();
        informativePanel = new javax.swing.JPanel();
        informativeLabel = new javax.swing.JLabel();
        playLocalMatchButton = new javax.swing.JButton();
        backFromLocalPanelButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(153, 204, 255));
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setPreferredSize(new java.awt.Dimension(600, 400));

        player1LocalLabel.setFont(new java.awt.Font("Andalus", 1, 18)); // NOI18N
        player1LocalLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        player1LocalLabel.setText("Player 1 :");

        player2LocalLabel.setFont(new java.awt.Font("Andalus", 1, 18)); // NOI18N
        player2LocalLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        player2LocalLabel.setText("Player 2 :");

        informativePanel.setBackground(new java.awt.Color(255, 255, 255));
        informativePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        informativeLabel.setFont(new java.awt.Font("Andalus", 0, 24)); // NOI18N
        informativeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        informativeLabel.setText("Enter the name of the players");
        informativeLabel.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

        javax.swing.GroupLayout informativePanelLayout = new javax.swing.GroupLayout(informativePanel);
        informativePanel.setLayout(informativePanelLayout);
        informativePanelLayout.setHorizontalGroup(
            informativePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informativePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(informativeLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        informativePanelLayout.setVerticalGroup(
            informativePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(informativeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
        );

        playLocalMatchButton.setBackground(new java.awt.Color(204, 204, 204));
        playLocalMatchButton.setFont(new java.awt.Font("Andalus", 1, 36)); // NOI18N
        playLocalMatchButton.setText("Play!");

        backFromLocalPanelButton.setBackground(new java.awt.Color(204, 204, 204));
        backFromLocalPanelButton.setFont(new java.awt.Font("Andalus", 1, 18)); // NOI18N
        backFromLocalPanelButton.setText("Back");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backFromLocalPanelButton)
                        .addGap(55, 55, 55)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(informativePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(player1LocalLabel)
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(player1LocalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(player2LocalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(player2LocalLabel)
                        .addComponent(playLocalMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(337, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(informativePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(backFromLocalPanelButton)))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(player1LocalLabel)
                    .addComponent(player1LocalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(player2LocalLabel)
                    .addComponent(player2LocalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addComponent(playLocalMatchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(208, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backFromLocalPanelButton;
    private javax.swing.JLabel informativeLabel;
    private javax.swing.JPanel informativePanel;
    private javax.swing.JButton playLocalMatchButton;
    private javax.swing.JLabel player1LocalLabel;
    private javax.swing.JTextField player1LocalTextField;
    private javax.swing.JLabel player2LocalLabel;
    private javax.swing.JTextField player2LocalTextField;
    // End of variables declaration//GEN-END:variables
    
    public JButton getBackFromLocalPanelButton() {
        return backFromLocalPanelButton;
    }

    public JButton getPlayLocalMatchButton() {
        return playLocalMatchButton;
    }

    public JTextField getPlayer1LocalTextField() {
        return player1LocalTextField;
    }

    public JTextField getPlayer2LocalTextField() {
        return player2LocalTextField;
    }


}

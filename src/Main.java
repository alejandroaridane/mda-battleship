
import battleship.ui.menu.MainFrame;
import controller.GameController;


public class Main {

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        GameController gameController = new GameController(mainFrame);
        mainFrame.setVisible(true);
    }
}

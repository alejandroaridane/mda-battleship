
package controller;

import battleship.ui.menu.MainFrame;
import game.Game;
import game.gameobjects.ships.Ship;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import map.MyTile;


public class GameController {
    private Game game;
    private final MainFrame view;
    private ActionListener newGameListener;

    public GameController(final MainFrame view) {
        this.view = view;
        setNewGameListener(view);
        view.initDeploymentMapForPlayer1();
        gameControllerBoatSelection();
    }

    private void setNewGameListener(final MainFrame view) {
        newGameListener = new ActionListener(){
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                createGame(view.getPlayer1Name(), view.getPlayer2Name());
                view.getLocalPlayer1DeploymentPanel1().getPlayer1NameLabel().setText(game.getActivePlayer().getName());
                view.changeToLocalPlayer1Deployment();
            }
            
        };
        
        view.initLocalMatchMenuPanelEvents(newGameListener);
    }
    
    public void shoot(int x, int y){
        game.getEnemyPlayer().getBoard().shoot(x, y);
    }
        
    public void createGame(String player1Name, String player2Name){
        game = new Game(player1Name, player2Name);
    }
    
    public void switchTurn(){
        game.switchTurn();
    }
    
    public void deployShip(Ship ship){
        game.getActivePlayer().getBoard().deployShip(ship);
    }
    
    
    private void gameControllerBoatSelection() {
        ArrayList<Integer> boatsSizeDeployment= new ArrayList<>(Arrays.asList(5,4,3,3,2,2));
        ArrayList<String> boatsNameDeployment= new ArrayList<>(Arrays.asList("Aircraft Carrier","Battleship","Cruiser","Submarine","Patrol Boat","Patrol Boat"));
        view.getLocalPlayer1DeploymentPanel1().initializeBoats(boatsSizeDeployment,boatsNameDeployment);  
    }
}


package game;

import game.gameobjects.Water;
import game.gameobjects.ships.Ship;
import game.gameobjects.ships.ShipSegment;

public class Board {
    
    Square[][] board;

    public Board() {
        board = waterBoard();
    }

    private Square[][] waterBoard() {
        Square[][] returnBoard = new Square[10][10];
        for (Square[] squares : returnBoard) {
            for (int i=0; i<squares.length; i++) {
                squares[i] = new Square();
                squares[i].setContent(new Water());
            }
        }
        
        return returnBoard;
    }
    
    public void shoot(int x, int y) {
        board[x][y].getContent().getShot();
    }
    
    public void deployShip(Ship ship) {
        int prowX = ship.getProw().getX();
        int prowY = ship.getProw().getY();
        
        if(prowX == ship.getStern().getX()){
            verticalShipDeployment(ship, prowX, prowY);
                    
        }else{
            horizontalShipDeployment(ship, prowX, prowY);
        }
    }

    private void horizontalShipDeployment(Ship ship, int prowX, int prowY) {
        if(ship.getProw().getX() < ship.getStern().getX()){
            for (ShipSegment segment : ship.getSegments()) {
                board[prowX++][prowY].setContent(segment);
            }
        }else{
            for (ShipSegment segment : ship.getSegments()) {
                board[prowX--][prowY].setContent(segment);
            }
        }
    }

    private void verticalShipDeployment(Ship ship, int prowX, int prowY) {
        if(prowY < ship.getStern().getY()){
            for (ShipSegment segment : ship.getSegments()) {
                board[prowX][prowY++].setContent(segment);
            }
        }else{
            for (ShipSegment segment : ship.getSegments()) {
                board[prowX][prowY--].setContent(segment);
            }
        }
    }

}

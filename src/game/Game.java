
package game;


public class Game {
    private Player activePlayer;
    private Player enemyPlayer;
    
    private Board deployBoard;
    private Board enemyBoard;

    public Player getActivePlayer() {
        return activePlayer;
    }

    public Player getEnemyPlayer() {
        return enemyPlayer;
    }

    public Game(String player1Name, String player2Name) {
        this.activePlayer = new Player(player1Name);
        this.enemyPlayer = new Player(player2Name);
        this.deployBoard = activePlayer.getBoard();
        this.enemyBoard = enemyPlayer.getBoard();
    }
    
    public void switchTurn(){
        Player swapPlayer = activePlayer;
        activePlayer = enemyPlayer;
        enemyPlayer = swapPlayer;
        
        Board swapBoard = deployBoard;
        deployBoard = enemyBoard;
        enemyBoard = swapBoard;
    }

    public void shoot(int x, int y) {
        enemyBoard.shoot(x, y);
        switchTurn();
    }
}


package game.gameobjects.ships;

import game.Square;


public class Battleship extends Ship{

    public Battleship(Square prow, Square stern) {
        super(prow, stern, 4);
    }

}

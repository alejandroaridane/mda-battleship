
package game.gameobjects.ships;

import game.Square;


public class Cruiser extends Ship{

    public Cruiser(Square prow, Square stern) {
        super(prow, stern, 3);
    }

}

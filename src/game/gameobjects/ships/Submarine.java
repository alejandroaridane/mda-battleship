
package game.gameobjects.ships;

import game.Square;


public class Submarine extends Ship{

    public Submarine(Square prow, Square stern) {
        super(prow, stern, 3);
    }

}

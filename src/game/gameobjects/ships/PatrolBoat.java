
package game.gameobjects.ships;

import game.Square;


public class PatrolBoat extends Ship{

    public PatrolBoat(Square prow, Square stern) {
        super(prow, stern, 2);
    }

}


package game.gameobjects.ships;

import game.Square;


public class AircraftCarrier extends Ship{

    public AircraftCarrier(Square prow, Square stern) {
        super(prow, stern, 5);
    }
    
}

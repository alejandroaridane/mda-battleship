
package game.gameobjects.ships;

import game.Square;
import java.util.ArrayList;

public abstract class Ship{
    protected final Square prow;
    protected final Square stern;
    protected final int size;
    
    protected ArrayList<ShipSegment> segments;

    public ArrayList<ShipSegment> getSegments() {
        return segments;
    }

    public Ship(Square prow, Square stern, int size) {
        this.prow = prow;
        this.stern = stern;
        this.size = size;
        for (int i = 0; i < size; i++) {
            segments.add(new ShipSegment());
        }
    }

    public Square getProw() {
        return prow;
    }

    public Square getStern() {
        return stern;
    }
    
    public int getSize() {
        return size;
    }
    
}


package game.gameobjects.ships;

import game.gameobjects.SquareObject;

public class ShipSegment extends SquareObject{
    boolean destroyed = false;

    public ShipSegment() {
    }
    
    @Override
    public void getShot(){
        destroyed = true;
    }
}

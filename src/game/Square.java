
package game;

import game.gameobjects.SquareObject;

public class Square {
    
    private int x;
    private int y;
    private SquareObject content;
    
    public void receiveShoot(){
        content.getShot();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setContent(SquareObject content) {
        this.content = content;
    }

    public SquareObject getContent() {
        return content;
    }
    
}
